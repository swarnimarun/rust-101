# Simple Rust 101 presentation

This presentation was created for presenting basic concepts of Rustlang under a properly contextualized manner.

Here's a list of topics covered in this presentation :->
- What's Rust?
- Why Rust?
- Installing Rust-stable, nightly and more....
- Concept of Ownership And Borrowing
- Mutability
- Jumping into Syntax
- Creating a simple CLI App in Rust