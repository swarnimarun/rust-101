# Rust 101
##### ->  [@color[orange](swarnimarun)](https://github.com/swarnimarun) @color[grey](or) [@color[orange](@steincodes)](https://twitter.com/steincodes)

---

## What's Rust?

Awesome system level programming language @color[gold]("like-C++") with @color[pink](static typing), @color[skyblue](reference counting) and @color[red](zero-cost abstraction)

---

## Why @size[1.2em]('not') C?

> “Having been tortured by C during my years of wiring security-critical software, I don’t think I exaggerate when I compare programming in it with walking through a minefield.” <br/>   @color[gold](by Ivan Ristic, Founder of SSL Labs)

---

## Why Rust?

Rust is a language that guarantees to deny memory leaks and data races by compile time checks without a Garbage Collector, using the concept of @color[gold](Ownership & Borrowing).

---

## Now, what is Ownership?

The concept of Ownership implies that there is always an owner of data. With emphasis on 'an'.

@size[0.9em](Here's the list of rules of Ownership in Rust,)
- @size[0.6em](Each value in Rust has a variable that’s called its owner.)
- @size[0.6em](There can only be one owner at a time.)
- @size[0.6em](When the owner goes out of scope, the value will be dropped.)

+++

## In Code

```rust
{
    let mut x = String::from("X");
    let mut y = String::from("Y");
    doSomething(&mut x);
    println!("The value of x is {} and y is {}", x, y); // error
    // As the ownership of x was transferred to another function
    // So this block has NO reference to x anymore
} 
// the value y is dropped, as they scope ends
```
---

## But, then what is Borrowing?

The concept of Borrowing refers to the ability to share immutable references without transferring ownership.
@size[0.9em](The rules for Borrowing or References,)
- @size[0.6em](At any given time, you can have either one mutable reference or any number of immutable references.)
- @size[0.6em](References must always be valid.)

+++ 

## In Code

```rust
{
    let mut x = 10;
    doSomething(&x); // borrowing of immutable reference
    println!("The value of x is {}", x); 
    // this time it works as intended
}
```

---

## Mutability

In Rust the variables by default are defined to be immutable.

```rust
let y = 32;  // this is immutable
let mut x = 23; // this is mutable
x = 45;
y = 54; // this will raise an error
```

---
## The Syntax?...

The syntax of rust is quite similar to that seen in JS,

```rust
// function prototype
fn main() {
    if true {  // conditionals are the usual
        println!("Hello World");
    }// println! is a Macro that prints text to the console
}
```
+++

## Data Types

Rust has all the usual data types and more. Some of them are,

```
i8, i16, i32, i64                 => signed integers
u8, u16, u32, u64                 => unsigned integers
f32, f64                          => floating points
char, str                         => characters and strs
slices, arrays, vectors           => collections
```

+++

## Pattern Matching and Enums

```rust
//enum
enum Vehicle {
    Car,
    Bike
}
// match
fn count_wheels(automobile : Vehicle) {
    match automobile {
        Vehicle::Car => 4,
        Vehicle::Bike => 2
    }
}
```

+++

## Loops

```rust
// for-loops are similar to for loops in python
for x in 1..10 {
    println!("The square of {} is {}", x, x*x);
}
```

+++

## Functions

Functions in Rust need a return type, for functions without a return type it defaults to void.

```rust
// functions are defined with fn
// and return type for functions are defined after ->
fn max_val(x: i8, y: i8) -> i8 {
    if x > y {
        x
    }
    else {
        y
    }
    // you return in rust by just writing it w/o semicolons
}
```

+++

## Macros

Macros are an integral part of Rust, they are heavily used in the standard libs.
@size[0.9em](Rust as of the Rust 2018 has 2 types of macros in general,)
- @size[0.6em](Declarative Macros)
- @size[0.6em](Procedural Macros)

```rust
// declarative macro example
macro_rules! foo {
    ($e:expr) => (println!("Foo me {}", $e));
}
// macros end with a ! 
fn main() {
    foo!("Hello");
}
```

---

## Rust has 'no' Classes...

Rust has the concept of Composition-over-Inheritance and while doing so it removes the headache of classes and tedious inheritance.

+++

## Rust has struct, impl and traits

Struct in Rust functions almost exactly like a struct in C

They hold variables while the methods are added in an impl. 
Which can also use traits similar to how interfaces are used. 

+++

## In Code

```rust
struct Rectangle {
    length : f32,
    breadth : f32
}
impl Rectangle {
    fn get_area(&self) -> f32 {
        self.length * self.breadth
    }
    fn get_perimeter(&self) -> f32 {
        (self.length + self.breadth) * 2.0
    }
}
```
---

## Cargo

Cargo is the build and package manager for Rust which makes creating and deploying projects in Rust really easy.

As a comparison it can be considered an equivalent of npm or pip.

+++

## Creating projects with 'cargo'

Cargo is a very easy to use tool and makes working in Rust a breeze.
```bash
cargo new project_name --bin
cd project_name
cargo build
cargo run
```

+++

## 'Cargo.toml' file

This is a file that gives cargo all the information about the project and list of dependencies it should fetch.

```toml
[package]
name = "name_of_project"
version = "x.x.x"
authors = ["Author Name <author-email-address>"]
edition = "2018"


[dependencies]
name-of-dependency = "version"
```

---

## How/Where to Rust?

Keep scrolling down for a list of projects and resources using which you can use Rust in various fields, Right now!!

+++

## Web Development

We have an interesting Web Framework for Rust called Rocket. Created by Sergio Benitez. 

https://rocket.rs

![rocket-logo](https://rocket.rs/v0.4/images/logo.svg)

+++

## Game Development

Rust is an awesome language for game development, because of it's lack of GC and pythonic syntax.

Some projects to look at if you want to do Game Development with Rust would be Amethyst Engine and few more.

For more information/resources check out, @color[gold](https://arewegameyet.com)

+++

## System Level Programming

There is a nice little OS called Redox, and when I say OS I don't mean only kernel, but Redox is a complete OS.

Redox is based on the micro-kernel design similar to older versions of Windows. And has a nice little ecosystem of apps.

@color[gold](https://github.com/redox-os/redox)


+++

## Webassembly + FFI

Rust is probably the best language to use WebAssembly, and has pretty good bindings for C.

This all results in Rust being able to be used in various situations as an integrated component rather than requiring a re-write of the systems.

---
# Questions??
+++

## Thank You!!

#### Reach me at [@color[orange](@steincodes)](https://twitter.com/steincodes) on Twitter.
#### Or Mail me, @color[gold](swarnimarun11@gmail.com)


+++

## For Links check out my gitlab(will move it all to github soon)

#### https://gitlab.com/swarnimarun/
#### https://github.com/swarnimarun/